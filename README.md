# Final Project

## Topic
Banking Crowd Academy Platform Epic 1 & 3

## High Level Architecture
![High Level Architecture Diagram](.img/diagram.png)

## Tech Stacks  
Backend Framework:  
 - Spring boot  

Authentication & Authorization:  
 - JSON Web Tokens  

Database:  
 - PostgreSQL (SQL)  
 - MongoDB (No-SQL)  
 
API Communication:  
 - RestTemplate  
 - Apache Kafka  


## Links
- [Repository](https://gitlab.com/gaditya-final-project)
- [Power Point](https://docs.google.com/presentation/d/1aI_uZLJAuwOQgjDfW6cbLNyt-UA9HwKrOemgjtSrddQ/edit?usp=sharing)
- [High Level Arhcitecture Diagram](https://whimsical.com/final-project-high-level-architecture-PjHtSQQN6sTW4FwYrJD68E)
- [ERD](https://cutt.ly/WYoEfZi)